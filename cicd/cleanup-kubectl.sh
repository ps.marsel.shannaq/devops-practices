#!/bin/bash

echo "##################################"
echo "Generate kubeconfig file"
echo "##################################"
echo "${KUBE_CONFIG}"|base64 -d > ~/.kube/config 


echo "##################################"
echo "Cleanup deployment"
echo "##################################"
NAMESPACE=$(echo "${CI_COMMIT_REF_NAME}"|cut -c 1-50)
kubectl delete namespace $NAMESPACE
echo "##################################"
echo "###Done###"
echo "##################################"