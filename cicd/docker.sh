#!/bin/sh

#COMMIT_SHA=$(git rev-parse --short=8 HEAD)
COMMIT_SHA=${CI_COMMIT_SHORT_SHA}
COMMIT_SHA_DATE=$(date +%y%m%d -d @`git show -s --format=%at $COMMIT_SHA` )

export RELEASE_VERSION=${COMMIT_SHA_DATE}-${COMMIT_SHA}

echo "##################################"
echo "### copy mvn target to docker ###"
echo "##################################"
cp artifacts/*.jar docker/app.jar

echo "##################################"
echo "Build Docker Image $RELEASE_VERSION"
echo "##################################"
docker build -t mshannaq/devops-assignment:${RELEASE_VERSION} docker

echo "##################################"
echo "Docker Login"
echo "##################################"
DOCKER_PASS=$(echo "UEBzc3cwcmQxMjMK"| base64 -d)
docker login -u mshannaq -p ${DOCKER_PASS}

echo "##################################"
echo "Push Docker Image $RELEASE_VERSION"
echo "##################################"
docker push mshannaq/devops-assignment:${RELEASE_VERSION}