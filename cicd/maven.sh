#!/bin/bash

#COMMIT_SHA=$(git rev-parse --short=8 HEAD)
COMMIT_SHA=${CI_COMMIT_SHORT_SHA}
COMMIT_SHA_DATE=$(date +%y%m%d -d @`git show -s --format=%at $COMMIT_SHA` )

export RELEASE_VERSION=${COMMIT_SHA_DATE}-${COMMIT_SHA}

echo "##################################"
echo "Build Maven Project $RELEASE_VERSION"
echo "##################################"
mvn clean install versions:set -DnewVersion=${RELEASE_VERSION}