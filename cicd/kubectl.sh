#!/bin/bash

echo "##################################"
echo "Generate kubeconfig file"
echo "##################################"
echo "${KUBE_CONFIG}"|base64 -d > ~/.kube/config 

echo "##################################"
echo "Create Namespace from ${CI_COMMIT_REF_NAME} branch"
echo "##################################"
NAMESPACE=$(echo "${CI_COMMIT_REF_NAME}"|cut -c 1-50)
kubectl create namespace $NAMESPACE

echo "##################################"
echo "get spring app tag"
echo "##################################"
COMMIT_SHA=${CI_COMMIT_SHORT_SHA}
COMMIT_SHA_DATE=$(date +%y%m%d -d @`git show -s --format=%at $COMMIT_SHA` )
export RELEASE_VERSION=${COMMIT_SHA_DATE}-${COMMIT_SHA}
sed -i 's|${RELEASE_VERSION}|'$RELEASE_VERSION'|g' kubernetes/deploy/app.yml

echo "##################################"
echo "Deploy spring app to $NAMESPACE namespace"
echo "##################################"
kubectl create -f kubernetes/deploy -n $NAMESPACE

echo "##################################"
echo "check spring app rediness"
echo "##################################"
APP_RESULT="true"
while $APP_RESULT
do 
APP_STATUS=$(kubectl logs  -l app=spring -n $NAMESPACE|grep "Completed initialization"|cut -c 98-121)
if [ "$APP_STATUS" = "Completed initialization" ]; then
   echo "Spring app ready"
   APP_RESULT="false"
else
  echo "Spring app not ready"
  sleep 10
fi
done

echo "##################################"
echo "you may access te spring app on"
echo "https://spring.local/actuator"
echo "##################################"