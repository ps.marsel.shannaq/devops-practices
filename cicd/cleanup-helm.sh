#!/bin/bash

echo "##################################"
echo "Generate kubeconfig file"
echo "##################################"
mkdir $HOME/.kube
echo "${KUBE_CONFIG}"|base64 -d > $HOME/.kube/config  

echo "##################################"
echo "Get Namespace from ${CI_COMMIT_REF_NAME} branch"
echo "##################################"
NAMESPACE=$(echo "${CI_COMMIT_REF_NAME}"|cut -c 1-50)


echo "##################################"
echo "Helm Delete spring app"
echo "##################################"
export TILLER_NAMESPACE=kube-system
HELM_RELEASE_NAME=$(echo "spring-app-$NAMESPACE"|cut -c 1-50)
helm delete --purge $HELM_RELEASE_NAME 

echo "##################################"
echo "Delete Namespace"
echo "##################################"
kubectl delete ns $NAMESPACE

echo "##################################"
echo "Done"
echo "##################################"