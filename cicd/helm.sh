#!/bin/bash

echo "##################################"
echo "Generate kubeconfig file"
echo "##################################"
mkdir $HOME/.kube
echo "${KUBE_CONFIG}"|base64 -d > $HOME/.kube/config 

echo "##################################"
echo "Get Namespace from ${CI_COMMIT_REF_NAME} branch"
echo "##################################"
NAMESPACE=$(echo "${CI_COMMIT_REF_NAME}"|cut -c 1-50)


echo "##################################"
echo "get spring app tag"
echo "##################################"
COMMIT_SHA=${CI_COMMIT_SHORT_SHA}
COMMIT_SHA_DATE=$(date +%y%m%d -d @`git show -s --format=%at $COMMIT_SHA` )
export RELEASE_VERSION=${COMMIT_SHA_DATE}-${COMMIT_SHA}

echo "##################################"
echo "Helm Deploy spring app"
echo "##################################"
export TILLER_NAMESPACE=kube-system
HELM_RELEASE_NAME=$(echo "spring-app-$NAMESPACE"|cut -c 1-50)
stern -n $NAMESPACE $HELM_RELEASE_NAME &
STERN_PID=$!
helm upgrade --install $HELM_RELEASE_NAME --namespace=$NAMESPACE helm/app --set image.tag="${RELEASE_VERSION}" --wait --timeout=900 --force
kill -9 ${STERN_PID}