#!/bin/bash

echo "###Starting Docker-Compose###"
docker-compose up -d
echo "###Plz access the application on http://localhost:8090/actuator###"
echo "###Done###"