#!/bin/bash

echo "###Stopping Docker-Compose###"
docker-compose stop
echo "###Done###"

echo "###Deleting Docker-Compose###"
docker-compose rm  --force 
echo "###Done###"