#!/bin/bash


echo "##################################"
echo "deploying prometheus-operator"
echo "##################################"
helm install stable/prometheus-operator --name prometheus-operator --namespace monitoring --wait --timeout=900 --force

echo "##################################"
echo "Done
echo "##################################"