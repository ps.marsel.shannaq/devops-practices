#!/bin/bash

echo "##################################"
echo "deploying elasticsearch"
echo "##################################"
helm upgrade --install elasticsearch elasticsearch --namespace=elk --wait --timeout=900 --force

echo "##################################"
echo "deploying filebeat"
echo "##################################"
helm upgrade --install filebeat filebeat --namespace=elk --wait --timeout=900 --force

echo "##################################"
echo "deploying kibana"
echo "##################################"
helm upgrade --install kibana kibana --namespace=elk --wait --timeout=900 --force

echo "##################################"
echo "Done"
echo "##################################"