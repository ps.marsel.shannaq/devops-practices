#!/bin/bash

rm start.local.com.crt start.local.com.csr start.local.com.key

openssl genrsa -out start.local.com.key 2048
openssl req -new -key start.local.com.key -out start.local.com.csr -subj "/C=QA/ST=Doha/L=Doha/O=ProgressSoft/OU=PS/CN=*.local"
openssl x509 -req -days 3650 -in start.local.com.csr -signkey start.local.com.key -out start.local.com.crt

