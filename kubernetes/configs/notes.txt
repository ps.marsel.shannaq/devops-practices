k create configmap check-db --from-file check-db.sh --dry-run=client -o yaml >app-configmap.yml

k create deployment spring --image mshannaq/devops-assignment:200903-7169d8e5 --dry-run=client -o yaml > app.yml
k create deployment mysql --image mysql:5.7 --dry-run=client -o yaml > db.yml
k expose deployment mysql --port 3306 --name mysql --dry-run=client -o yaml >db-svc.yml
k expose deployment spring --port 8080 --name=spring-svc --type=NodePort --dry-run=client -o yaml > app-svc.yml

k create secret tls default-crt --cert start.local.com.crt --key start.local.com.key --dry-run=client -o yaml >nginx-secret.yaml


